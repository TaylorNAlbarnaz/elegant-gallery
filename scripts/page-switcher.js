const imageGrid = document.getElementById("image-grid");
const pageCounter = document.getElementById("page-counter");
let pages = null;
let pageSelectors = null;

// Lista de imagens
let imageList = Array(48).fill("https://th.bing.com/th/id/OIP.U1q1EABXSd-k7S0RDN1mNQHaLS?pid=ImgDet&rs=1");

// Troca de página ao clicar em um seletor
function changePage(pageIndex) {
    return function() {
        // Pega o id das divs da página e seletor atuais
        const pageId = "p-" + pageIndex;
        const selectorId = "ps-" + pageIndex;
        console.log(pageIndex);

        // Diminui a opacidade de todas as páginas exceto a atual
        for (page of pages) {
            page.style.opacity = "0.7";
        }
        document.getElementById(pageId).style.opacity = "1";

        // Desseleciona todos os seletores, depois seleciona o atual
        for (selector of pageSelectors) {
            selector.classList.remove("selected");
        }
        document.getElementById(selectorId).classList.add("selected");

        // Calcula a posição da tela e movimenta a grida
        const gridTranslation = pageIndex * 100;
        imageGrid.style.transform = `translateX(-${gridTranslation}%)`;
    }
}

// Cria o grid de imagens dinâmicamente
function createImageGrid() {
    // Calcula quantas páginas seram necessárias
    const pageCount = Math.ceil(imageList.length / 20);

    // Cria as páginas com suas respectivas imagens
    for (i=0; i<pageCount; i++) {
        // Cria o elemento da página
        const pageElement = document.createElement("div");
        pageElement.classList.add("page");
        pageElement.id = "p-" + i;

        // Cria todas as imagens coloca elas na página
        Array.from({length: 20}, () => {
            const nextImage = imageList.shift();
            if (nextImage) {
                const imageDiv = document.createElement("div");
                imageDiv.classList.add("item");
                imageDiv.style.backgroundImage = `url(${nextImage})`;
                pageElement.appendChild(imageDiv);
            }
        })

        // Adiciona a página ao grid
        imageGrid.appendChild(pageElement);

        // Cria o selector da página e adiciona ao counter
        const pageSelector = document.createElement("span");
        pageSelector.textContent = "•";
        pageSelector.classList.add("selector");
        pageSelector.id = "ps-" + i;
        pageCounter.appendChild(pageSelector);
    }

    // Seleciona o primeiro selector caso ele exista
    const firstSelector = document.getElementById("ps-0");
    if (firstSelector)
        firstSelector.classList.add("selected");
}

// Atualiza as variáveis que contem a lista de páginas e seletores
function getPagesAndSelectors() {
    pages = document.getElementsByClassName("page");
    pageSelectors = document.getElementsByClassName("selector");
}

// Adiciona os eventListeners aos elementos das páginas
function addPageEvents() {
    // Adiciona o evento para trocar de página à cada seletor
    for (i=0; i<pageSelectors.length; i++) {
        pageSelectors[i].addEventListener('click', changePage(i));
    }
}

createImageGrid();
getPagesAndSelectors();
addPageEvents();